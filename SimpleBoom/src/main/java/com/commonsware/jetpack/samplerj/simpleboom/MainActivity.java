/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.simpleboom;

import android.os.Bundle;
import android.os.SystemClock;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
  private Button showElapsed;
  private long startTimeMs = SystemClock.elapsedRealtime();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_main);
    showElapsed = findViewById(R.id.showElapsed);
    showElapsed.setOnClickListener(v -> updateButton());
  }

  void updateButton() {
    long nowMs = SystemClock.elapsedRealtime();
    int seconds = (int)((nowMs - startTimeMs) / 1000);

    showElapsed.setText(seconds);
  }
}
