/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.activities;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

class ColorViewHolder extends RecyclerView.ViewHolder {
  private final View swatch;
  private final TextView label;
  private int color;

  ColorViewHolder(View row) {
    super(row);

    swatch = row.findViewById(R.id.swatch);
    label = row.findViewById(R.id.label);
    row.setOnClickListener(this::showBigSwatch);
  }

  void bindTo(Integer color) {
    this.color = color;

    label.setText(label.getContext().getString(R.string.label_template, color));
    swatch.setBackgroundColor(color);
  }

  private void showBigSwatch(View v) {
    Context context = v.getContext();

    context.startActivity(new Intent(context, BigSwatchActivity.class)
      .putExtra(BigSwatchActivity.EXTRA_COLOR, color));
  }
}
